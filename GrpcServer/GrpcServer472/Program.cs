﻿using Microsoft.Owin;
using Microsoft.Owin.Hosting;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: OwinStartup("ProductionConfiguration", typeof(GrpcServer472.ProductionStartup2))]

namespace GrpcServer472
{
    class Program
    {
        static void Main(string[] args)
        {
            ConfigureService.Configure();
        }
    }

    public class MyService
    {
        private IDisposable _owin;
        public void Start()
        {
            // write code here that runs when the Windows Service starts up.  
            _owin = WebApp.Start("http://+:5001/");
        }
        public void Stop()
        {
            _owin?.Dispose();
            // write code here that runs when the Windows Service stops.  
        }
    }

    public class ProductionStartup2
    {
        public void Configuration(IAppBuilder app)
        {
            app.Run(context =>
            {
                string t = DateTime.Now.Millisecond.ToString();
                return context.Response.WriteAsync(t + " 2nd Production OWIN App");
            });
        }
    }
}

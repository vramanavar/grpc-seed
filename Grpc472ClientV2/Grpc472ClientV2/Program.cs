﻿using Grpc.Core;
using GrpcClient;
//using GrpcClient472;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grpc472ClientV2
{
    class Program
    {
        static async Task Main(string[] args)
        {
            try
            {
                //SslCredentials secureCredentials = new SslCredentials(File.ReadAllText("GrpcServer.pem"));
                var channel = new Channel("127.0.0.1:5001", ChannelCredentials.Insecure);

                var client = new Greeter.GreeterClient(channel);
                var helloRequest = new HelloRequest { Name = "GreeterClient" };
                var reply = await client.SayHelloAsync(helloRequest);

                Console.WriteLine("Greeting: " + reply.Message);
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
            catch (Exception excp)
            {
                Console.Write(excp.ToString());
            }
        }
    }
}

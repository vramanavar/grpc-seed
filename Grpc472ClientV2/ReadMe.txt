﻿1) openssl can be found in Git Installation path; set this in Env Path variable

2) Run the GRPC Server and export the certificate from Browser address bar

3) Use below command to extract the public key to be used in Grpc Client app

openssl x509 -inform der -in GrpcServer.cer -pubkey -noout > GrpcServer.pem

4) Sequence of packages installation
	a)	Google.Protobuf
	b)	Grpc (This pulls in Grpc.Core and Grpc.Core.Api)
	c)	Grpc.Tools	(Once this is installed; restart Visual Studio; then "BuildAction" filetype of "Protobuf" starts to appear)

5)	Copy the .proto file from the server

6)	Compile the project and it should be good to go